module no.ntnu.idatx2001.oblig3.cardgame.cardgame {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    opens no.ntnu.idatt2001.oblig3.cardgame.view to javafx.fxml;
    exports no.ntnu.idatt2001.oblig3.cardgame.model;
    opens no.ntnu.idatt2001.oblig3.cardgame.model to javafx.fxml;
    exports no.ntnu.idatt2001.oblig3.cardgame.controller;
    opens no.ntnu.idatt2001.oblig3.cardgame.controller to javafx.fxml;
    exports no.ntnu.idatt2001.oblig3.cardgame.view;
    exports no.ntnu.idatt2001.oblig3.cardgame;
    opens no.ntnu.idatt2001.oblig3.cardgame to javafx.fxml;

}