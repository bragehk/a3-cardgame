package no.ntnu.idatt2001.oblig3.cardgame;


import java.util.ArrayList;
import java.util.Random;

/**
 * This class keeps track of a deck of cards.
 *
 * @author Brage Halvorsen Kvamme
 * @version 0.1
 */
public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private final ArrayList<PlayingCard> cards = new ArrayList<>();

    /**
     * Instantiates a new Deck of cards.
     */
    public DeckOfCards() {
        for(int i = 0; i<4; i++){
            for(int j = 1; j<14; j++){
                cards.add(new PlayingCard(suit[i],j));
            }
        }
    }

    /**
     * Deals n cards
     *
     * @param n number of cards you want to deal
     * @return the array list
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {
        if (n<0) throw new IllegalArgumentException("It's not possible to deal a negative amount of cards.");
        if (n>52) throw new IllegalArgumentException("There are not more cards than 52 in a deck.");

        Random random = new Random();
        int randInt = random.nextInt(52);

        ArrayList<PlayingCard> randomCards = new ArrayList<>();

        for(int i = 0; i<n; i++){
            while (randomCards.contains(cards.get(randInt))) {
                randInt = random.nextInt(52);
            }
            randomCards.add(cards.get(randInt));
            randInt = random.nextInt(52);
        }
        return randomCards;
    }

    /**
     * Gets all cards in a deck.
     *
     * @return the cards
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }


}
