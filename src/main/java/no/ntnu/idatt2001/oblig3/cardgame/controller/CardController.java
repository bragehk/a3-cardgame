package no.ntnu.idatt2001.oblig3.cardgame.controller;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.idatt2001.oblig3.cardgame.DeckOfCards;
import no.ntnu.idatt2001.oblig3.cardgame.PlayingCard;
import no.ntnu.idatt2001.oblig3.cardgame.model.Model;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Card controller for the GUI.
 *
 * @author Brage Halvorsen Kvamme
 * @version 0.1
 */
public class CardController implements Initializable {
    private final DeckOfCards deck = new DeckOfCards();
    private ArrayList<PlayingCard> cards = new ArrayList<>();

    /**
     * Initializes the scene. First hand is a royal flush to demonstrate that the flush method is working.
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        faceSum.setEditable(false);
        flushText.setEditable(false);
        heartText.setEditable(false);
        isQueenText.setEditable(false);
        startingCards();
        onCheckButtonClick();
    }

    private void startingCards() {
        ArrayList<PlayingCard> startCards = new ArrayList<>();
        startCards.add(new PlayingCard('H',13));
        startCards.add(new PlayingCard('H',12));
        startCards.add(new PlayingCard('H',11));
        startCards.add(new PlayingCard('H',10));
        startCards.add(new PlayingCard('H',1));

        card1.setImage(new Image(new PlayingCard('H',13).getImageLocation()));
        card2.setImage(new Image(new PlayingCard('H',12).getImageLocation()));
        card3.setImage(new Image(new PlayingCard('H',11).getImageLocation()));
        card4.setImage(new Image(new PlayingCard('H',10).getImageLocation()));
        card5.setImage(new Image(new PlayingCard('H',1).getImageLocation()));
        cards = startCards;
    }


    @FXML
    private TextField faceSum;

    @FXML
    private TextField flushText;

    @FXML
    private TextField heartText;

    @FXML
    private TextField isQueenText;

    @FXML
    private ImageView card1;

    @FXML
    private ImageView card2;

    @FXML
    private ImageView card3;

    @FXML
    private ImageView card4;

    @FXML
    private ImageView card5;

    /**
     * On deal button click.
     */
    @FXML
    private void onDealButtonClick() {
        cards = deck.dealHand(5);
        card1.setImage(new Image(cards.get(0).getImageLocation()));
        card2.setImage(new Image(cards.get(1).getImageLocation()));
        card3.setImage(new Image(cards.get(2).getImageLocation()));
        card4.setImage(new Image(cards.get(3).getImageLocation()));
        card5.setImage(new Image(cards.get(4).getImageLocation()));
        isQueenText.clear();
        flushText.clear();
        faceSum.clear();
        heartText.clear();
    }

    /**
     * Statistics about the cards will update when the check button is pressed.
     *
     */
    @FXML
    private void onCheckButtonClick() {
        Model model = new Model(cards);
        if(model.containsQueenOfSpades()) isQueenText.setText("Yes");
        else isQueenText.setText("No");

        if(model.isFlush()) flushText.setText("Yes");
        else flushText.setText("No");

        faceSum.setText(Integer.toString(model.sumOfCards()));

        heartText.setText(model.getHearts().replace("[","").replace("]",""));
    }
}