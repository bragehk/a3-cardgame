package no.ntnu.idatt2001.oblig3.cardgame.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main Card Application class for starting the Card Application GUI.
 *
 * @author Brage Halvorsen Kvamme
 * @version 0.2
 */
public class CardApp extends Application {

    /**
     * This method creates a Stage and loads up the scene.
     *
     * @param stage Stage provided by app.
     * @throws IOException
     */
    @Override
    public void start(Stage stage) {
        final int MIN_HEIGHT = 600;
        final int MIN_WITH = 640;
        FXMLLoader fxmlLoader = new FXMLLoader(CardApp.class.getClassLoader().getResource("CardGame.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage.setMinWidth(MIN_WITH);
        stage.setMinHeight(MIN_HEIGHT);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main method of the application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch();
    }
}