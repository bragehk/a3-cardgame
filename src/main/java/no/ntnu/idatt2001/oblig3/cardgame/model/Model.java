package no.ntnu.idatt2001.oblig3.cardgame.model;

import no.ntnu.idatt2001.oblig3.cardgame.PlayingCard;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author Brage halvorsen Kvamme
 * @version 0.2
 */
public class Model {
    private final ArrayList<PlayingCard> cards;

    /**
     * Instantiates a new Model that does the logic in this application.
     *
     * @param cards the cards you want information from.
     */
    public Model(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     * Instantiates a new Model with no cards.
     */
    public Model() {
        this.cards = new ArrayList<>();
    }

    /**
     * Sum of the faces in this model.
     *
     * @return the som of faces.
     */
    public int sumOfCards() {
        return cards.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * Gets every heart in this model.
     *
     * @return hearts in this model.
     */
    public String getHearts() {
        List<PlayingCard> hearts = cards.stream().filter(p -> p.getSuit() == 'H').toList();
        if(hearts.isEmpty()) {
            return "No Hearts";
        } else {
            return hearts.toString();
        }
    }

    /**
     * Contains queen of spades.
     *
     * @return true if queen of spades is present, false if not.
     */
    public boolean containsQueenOfSpades() {
        return cards.stream().anyMatch(playingCard -> playingCard.getAsString().equals("S12"));
    }

    /**
     * Is flush boolean.
     *
     * @return true if flush, false if not.
     */
    public boolean isFlush() {
        return cards.stream()
                .map(PlayingCard::getSuit)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .anyMatch(p -> p.getValue() > 4);
    }

    /**
     * Gets cards in the model.
     *
     * @return the cards in the model.
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }
}
