package no.ntnu.idatt2001.oblig3.cardgame;

import no.ntnu.idatt2001.oblig3.cardgame.model.Model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ModelTest {

    @Nested
    public class Methods{
        ArrayList<PlayingCard> cards = new ArrayList<>();
        Model model;

        @BeforeEach
        public void createCards() {
            cards.add(new PlayingCard('S',3));
            cards.add(new PlayingCard('H',4));
            cards.add(new PlayingCard('D',6));
            cards.add(new PlayingCard('C',7));
            cards.add(new PlayingCard('S',12));
            model = new Model(cards);
        }

        @Test
        public void sumOfCardsTest() {
            assertEquals(32, model.sumOfCards());
        }

        @Test
        public void getHeartsTest() {
            assertEquals("[H4]", model.getHearts());
        }

        @Test
        public void containsQueenOfSpadesTrueTest() {
            assertTrue(model.containsQueenOfSpades());
        }

        @Test
        public void containsQueenOfSpadesFalseTest() {
            Model model2 = new Model(new ArrayList<>());
            assertFalse(model2.containsQueenOfSpades());
        }

        @Test
        public void isNotFlushTest() {
            assertFalse(model.isFlush());
        }

        @Test
        public void isFlushTest() {
            cards.add(new PlayingCard('S',1));
            cards.add(new PlayingCard('S',11));
            cards.add(new PlayingCard('S',6));
            model = new Model(cards);

            assertTrue(model.isFlush());
        }
    }


}
