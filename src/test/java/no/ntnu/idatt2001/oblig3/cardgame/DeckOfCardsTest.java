package no.ntnu.idatt2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    @Test
    public void thereAre52CardsInDeck() {
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52,deck.getCards().size());
    }

    @Test
    public void deal52CardsTest() {
        DeckOfCards deck = new DeckOfCards();
        deck.dealHand(52);
    }

    @Test
    public void deal53CardsTest() {
        DeckOfCards deck = new DeckOfCards();
        assertThrows(Exception.class, () -> deck.dealHand(53));
    }

    @Test
    public void noCardsAreEqualTest() {
        /*
        This method checks that no cards are equal.
        A assertFalse test is there to show that the lambda expression works.
         */
        DeckOfCards deck = new DeckOfCards();

        ArrayList<PlayingCard> duplicate = new ArrayList<>();
        PlayingCard card = new PlayingCard('H',3);
        duplicate.add(card);
        duplicate.add(card);

        assertTrue(deck.getCards().stream()
                .allMatch(new HashSet<PlayingCard>()::add));
        assertFalse(duplicate.stream()
                .allMatch(new HashSet<PlayingCard>()::add));
    }
}
